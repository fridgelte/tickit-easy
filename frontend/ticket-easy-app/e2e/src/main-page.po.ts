import { browser, by, element } from 'protractor';

export class MainPage {
  navigateTo(): Promise<any> {
    return browser.get(browser.baseUrl) as Promise<any>;
  }

  getTitleText(): Promise<string> {
    return element(by.css('#welcome')).getText() as Promise<string>;
  }
}