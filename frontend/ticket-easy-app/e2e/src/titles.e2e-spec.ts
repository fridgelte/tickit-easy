import { Titles } from './titles.po';

describe('Title service tests', () =>{
    let titles : Titles;
    beforeEach(() => {
        titles = new Titles();
    });

    it('when user is open the main page, he should see the title Tick It Easy!', ()=>{
        titles.navigateToMainPage();
        expect(titles.getMainPageTitle()).toEqual('Tick It Easy!');
    });

    it('when user is navigate to purchase tickets page, he should see the title Purchase Tickets! | Tick It Easy!', ()=>{
        titles.navigateToPurchaseTickets();
        expect(titles.getPurchaseTicketsPageTitle()).toEqual('Purchase Tickets! | Tick It Easy!');
    });
})