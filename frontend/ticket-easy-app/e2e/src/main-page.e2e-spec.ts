import { browser } from 'protractor';
import { MainPage } from './main-page.po';
import {Title} from "@angular/platform-browser";

describe('MainPageComponent tesztek', () =>{
    let mainPage : MainPage;
    let titleService: Title;
    beforeEach(() => {
        mainPage = new MainPage();
    });

    it('when user is open the main page, he should see a welcome text Hello!', ()=>{
        console.log("Belép a főoldalra");
        mainPage.navigateTo();
        expect(mainPage.getTitleText()).toEqual('Hello!')
    });
})