import { browser, by, element } from 'protractor';

export class Titles {
  navigateToMainPage(): Promise<any> {
    return browser.get(browser.baseUrl) as Promise<any>;
  }

  navigateToPurchaseTickets():Promise<any>{
    return browser.get(browser.baseUrl+'/purchase') as Promise<any>;
  }

  getMainPageTitle(): Promise<string> {
    return browser.getTitle() as Promise<string>;
  }

  getPurchaseTicketsPageTitle(): Promise<string> {
    return browser.getTitle() as Promise<string>;
  }
}