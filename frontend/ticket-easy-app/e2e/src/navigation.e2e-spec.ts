import { browser, element, by } from 'protractor';

describe('Navigation tests', () =>{
    beforeEach(() => {
        browser.get(browser.baseUrl);
    });

    it('when user is on the main page, he should navigate to purchase tickets page', ()=>{
        element(by.css(".menu-button")).click().then(function(){
            element(by.id('purchase-tickets-page-routing')).click().then(function(){
                browser.sleep(2000).then(function(){
                  browser.getCurrentUrl().then(function(actualUrl){
                    expect(actualUrl.indexOf('purchase') !== -1).toBeTruthy();
                });
        
            });
        });
    });

    it('when user is on the purchase tickets page, he should navigate to main page', ()=>{
        element(by.css(".menu-button")).click().then(function(){
            element(by.id('main-page-routing')).click().then(function(){
                browser.sleep(2000).then(function(){
                  browser.getCurrentUrl().then(function(actualUrl){
                    expect(browser.baseUrl==actualUrl).toBeTruthy();
                  });
                });
        
            });
        });
    });
})