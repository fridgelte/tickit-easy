import { Component, OnInit, Input } from '@angular/core';
import { TravelCard } from '../classes/TravelCard';

@Component({
  selector: 'app-travel-card',
  templateUrl: './travel-card.component.html',
  styleUrls: ['./travel-card.component.scss']
})
export class TravelCardComponent implements OnInit {

  @Input() travelCards  : TravelCard[];
  
  constructor() { 
  }

  ngOnInit(): void {
  }

}
