import { Component, OnInit,Inject } from '@angular/core';
import {Title} from "@angular/platform-browser";
import { TravelCard } from '../classes/TravelCard';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { AuthService } from '../auth.service';

export interface DialogData {
  type: string;
  name: string;
}

@Component({
  selector: 'app-purchase-tickets',
  templateUrl: './purchase-tickets.component.html',
  styleUrls: ['./purchase-tickets.component.scss']
})
export class PurchaseTicketsComponent implements OnInit {

  travelCards: TravelCard[];
  cart: TravelCard[];
  sumPrice : number = 0;
  ticketCount: number = 0;
  constructor(private titleService:Title, public dialog: MatDialog,public auth: AuthService) {
      this.titleService.setTitle("Purchase Tickets! | Tick It Easy!");
      this.travelCards = new Array();
      this.cart = new Array();
      this.travelCards.push(new TravelCard("Budapest-Keszthely by bus","2020.04.03.",2000,2));
      this.travelCards.push(new TravelCard("Budapest-Keszthely by train","2020.04.03.",2000,15));
      this.travelCards.push(new TravelCard("Budapest-Keszthely by plane","2020.04.03.",2000,15));
      this.travelCards.push(new TravelCard("Szolnok-Siófok by plane","2020.04.03.",2000,15));
      this.travelCards.push(new TravelCard("Szolnok-Siófok by bus","2020.04.03.",2000,15));
      this.travelCards.push(new TravelCard("Budapest-Keszthely by bus","2020.04.03.",2000,15));
      this.travelCards.push(new TravelCard("Budapest-Keszthely by train","2020.04.03.",2000,15));
      this.travelCards.push(new TravelCard("Szolnok-Siófok by plane","2020.04.03.",2000,15));
      this.travelCards.push(new TravelCard("Budapest-Keszthely by bus","2020.04.03.",2000,15));
  }

  addItemToCart(card: TravelCard,index: number){
    const i = this.travelCards.indexOf(card);
    if(this.travelCards[i].available == 0){
      const dialogRef = this.dialog.open(DialogDataExampleDialog, {
        width: '250px',
        height: '250px',
        data: {name: '', type: 'No available tickets, you cant add this item to your'}
      });
      return;
    }
    this.cart.push(card);
    this.travelCards[i].available-=1;
    this.ticketCount = this.cart.length;
    const dialogRef = this.dialog.open(DialogDataExampleDialog, {
      width: '250px',
      height: '250px',
      data: {name: card.title+' is', type: 'is added to'}
    });
    console.log("size: "+this.cart.length);
    this.sumPrice+=card.price;
    let id = "card-"+index; 
    document.getElementById(id).style.background = "yellow";
  }

  deleteItemFromCart(card: TravelCard,index: number){
    const j:number = this.travelCards.indexOf(card);
    const i: number = this.cart.indexOf(card);
    if (i !== -1) {
        this.cart.splice(i, 1);
        this.travelCards[j].available+=1;
    }
    else{
      const dialogRef = this.dialog.open(DialogDataExampleDialog, {
        width: '250px',
        height: '200px',
        data: {name: card.title+' is', type: 'not in'}
      });
      return;
    }
    console.log("size: "+this.cart.length); 
    this.ticketCount = this.cart.length;
    const dialogRef = this.dialog.open(DialogDataExampleDialog, {
      width: '250px',
      height: '250px',
      data: {name: card.title+' is', type: 'is removed from'}
    });
    this.sumPrice-=card.price;
    const k: number = this.cart.indexOf(card);
    if (k == -1) {
      let id = "card-"+index; 
      document.getElementById(id).style.background = "white";
    }
  }

  notLoggedInDialog(){
    const dialogRef = this.dialog.open(DialogDataExampleDialog, {
      width: '250px',
      height: '200px',
      data: {name: '', type: 'You need to log in to manage your'}
    });
  }

  ngOnInit(): void {
  }
}

@Component({
  selector: 'dialog-data-example-dialog',
  templateUrl: 'dialog-data-example-dialog.html',
})
export class DialogDataExampleDialog {
  constructor(public dialogRef: MatDialogRef<DialogDataExampleDialog>,
              @Inject(MAT_DIALOG_DATA) public data: DialogData) {}
  onNoClick(): void {
    this.dialogRef.close();
  }
}


