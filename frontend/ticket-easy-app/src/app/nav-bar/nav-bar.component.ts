import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent implements OnInit {

  currentUser: string
  constructor(public auth: AuthService) { 
    auth.userProfile$.subscribe(x=> {
      this.currentUser = x;
      console.log("user"+this.currentUser);
    });
  }

  ngOnInit(): void {
  }
}
