export class TravelCard{
    title: string;
    date: string;
    price: number;
    available: number;
    constructor(title: string,date: string, price: number, available: number){
        this.title = title;
        this.date = date;
        this.price = price;
        this. available = available;
    }
}