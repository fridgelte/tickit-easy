import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MainPageComponent} from './main-page/main-page.component';
import { PurchaseTicketsComponent } from './purchase-tickets/purchase-tickets.component';
import { ProfileComponent } from './profile/profile.component';

const routes: Routes = [
  {path: '', component: MainPageComponent},
  {path: 'callback', component: MainPageComponent},
  {path:'purchase', component: PurchaseTicketsComponent},
  {path: 'profile', component: ProfileComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
